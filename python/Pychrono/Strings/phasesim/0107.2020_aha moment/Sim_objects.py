# -*- coding: utf-8 -*-
"""
Created on Thu Nov 14 17:56:34 2019

@author: dmulr
"""

import pychrono.core as chrono
import pychrono.irrlicht as chronoirr
import pychrono.postprocess as postprocess
import os
import numpy as np
import timeit
from pid_controller.pid import PID

# In[Create material]
def Material(mu_f,mu_b,mu_r,mu_s,C,Ct,Cr,Cs):
    material = chrono.ChMaterialSurfaceNSC()
    material.SetFriction(mu_f)
    material.SetDampingF(mu_b)
    material.SetCompliance (C)
    material.SetComplianceT(Ct)
    material.SetRollingFriction(mu_r)
    material.SetSpinningFriction(mu_s)
    material.SetComplianceRolling(Cr)
    material.SetComplianceSpinning(Cs)
    return material

# In[Create Floor]
def Floor(material,length,tall):
    body_floor = chrono.ChBody()
    body_floor.SetBodyFixed(True)
    body_floor.SetPos(chrono.ChVectorD(0, -tall, 0 ))
    body_floor.SetMaterialSurface(material)
    body_floor.GetCollisionModel().ClearModel()
    body_floor.GetCollisionModel().AddBox(length, tall, length) # hemi sizes
    body_floor.GetCollisionModel().BuildModel()
    body_floor.SetCollide(True)
    body_floor_shape = chrono.ChBoxShape()
    body_floor_shape.GetBoxGeometry().Size = chrono.ChVectorD(length, tall, length)
    body_floor.GetAssets().push_back(body_floor_shape)
    body_floor_texture = chrono.ChTexture()
    body_floor_texture.SetTextureFilename(chrono.GetChronoDataPath() + 'aluminum.jpg')
    body_floor.GetAssets().push_back(body_floor_texture)
    return(body_floor)

# In[IKnterior Granulars]
def Interior(x,y,z,i,diameter,height,rowp,R2,material,obj,my_system,body_floor):
    gran = chrono.ChBody()
    gran = chrono.ChBodyEasyCylinder(diameter/2, height,rowp)
    gran.SetPos(chrono.ChVectorD(x,y,z))
    gran.SetMaterialSurface(material)
    gran.SetId(i)
    gran.GetCollisionModel().ClearModel()
    gran.GetCollisionModel().AddCylinder(diameter/2,diameter/2,height/2) # hemi sizes
    gran.GetCollisionModel().BuildModel()
    gran.SetCollide(True)
    gran.SetBodyFixed(False)
    col_r = chrono.ChColorAsset()
    col_r.SetColor(chrono.ChColor(1, 0, 0))
    gran.AddAsset(col_r)
    pt=chrono.ChLinkMatePlane()
    pt.Initialize(body_floor,gran,False,chrono.ChVectorD(0,0,0),chrono.ChVectorD(0,0,0),chrono.ChVectorD(0,1, 0),chrono.ChVectorD(0,-1, 0))
    my_system.AddLink(pt)
    
    my_system.Add(gran)
    obj.append(gran)
    return my_system,obj 
    
# In[Ball]
def Ball(x,y,z,Rb,height,hhalf,rowr,material,obj,my_system,forceb,body_floor,Balls):
    
    z2x = chrono.ChQuaternionD()
    z2x.Q_from_AngAxis(chrono.CH_C_PI / 2, chrono.ChVectorD(0, 1, 0))
    ball = chrono.ChBody()
    ball = chrono.ChBodyEasyCylinder(Rb,height,rowr)
    ball.SetPos(chrono.ChVectorD(x,y,z))
    ball.SetMaterialSurface(material)
    myforcex = chrono.ChForce()
    ball.AddForce(myforcex)
    myforcex.SetMode(chrono.ChForce.FORCE)
    myforcex.SetDir(chrono.VECT_X)
    myforcex.SetVrelpoint(chrono.ChVectorD(x,.03*y,z))
    #myforcex.SetMforce(mag)
    forceb.append(myforcex)
    # collision model
    ball.GetCollisionModel().ClearModel()
    ball.GetCollisionModel().AddCylinder(Rb,Rb,hhalf) # hemi sizes
    ball.GetCollisionModel().BuildModel()
    ball.SetCollide(True)
    ball.SetBodyFixed(False)
    col_b = chrono.ChColorAsset()
    col_b.SetColor(chrono.ChColor(0, 0, 1))
    ball.AddAsset(col_b)
    pt=chrono.ChLinkMatePlane()
    pt.Initialize(body_floor,ball,False,chrono.ChVectorD(0,0,0),chrono.ChVectorD(0,0,0),chrono.ChVectorD(0,1, 0),chrono.ChVectorD(0,-1, 0))
    prismatic_ground_ball = chrono.ChLinkLockPrismatic()
    prismatic_ground_ball.SetName("prismatic_ground_ball")
    prismatic_ground_ball.Initialize(body_floor, ball, chrono.ChCoordsysD(chrono.ChVectorD(5.5, 0, 0), z2x))
    my_system.AddLink(prismatic_ground_ball)
    
    my_system.AddLink(pt)
    my_system.Add(ball)
    obj.append(ball)
    Balls.append(ball)
# In[Def Box]
def Box(x,y,z,Rb,RL,height,hhalf,rowr,material,obj,my_system,forceb,body_floor,ball):
    
    z2x = chrono.ChQuaternionD()
    z2x.Q_from_AngAxis(chrono.CH_C_PI/2, chrono.ChVectorD(0, 1, 0))
    ball = chrono.ChBody()
    ball = chrono.ChBodyEasyBox(2*Rb,height,2*RL,rowr)
    ball.SetPos(chrono.ChVectorD(x,y,z))
    ball.SetRot(chrono.Q_from_AngY(np.pi/4))
    ball.SetMaterialSurface(material)
    myforcex = chrono.ChForce()
    ball.AddForce(myforcex)
    myforcex.SetMode(chrono.ChForce.FORCE)
    myforcex.SetDir(chrono.VECT_X)
    myforcex.SetVrelpoint(chrono.ChVectorD(x,.03*y,z))
    #myforcex.SetMforce(mag)
    forceb.append(myforcex)
    # Collision shape
    ball.GetCollisionModel().ClearModel()
    ball.GetCollisionModel().AddBox(Rb,hhalf,Rb) # must set half sizes
    ball.GetCollisionModel().BuildModel()
    ball.SetCollide(True)
    ball.SetBodyFixed(False)
    col_b = chrono.ChColorAsset()
    col_b.SetColor(chrono.ChColor(0, 0, 1))
    ball.AddAsset(col_b)
    pt=chrono.ChLinkMatePlane()
    pt.Initialize(body_floor,ball,False,chrono.ChVectorD(0,0,0),chrono.ChVectorD(0,0,0),chrono.ChVectorD(0,1, 0),chrono.ChVectorD(0,-1, 0))
    prismatic_ground_ball = chrono.ChLinkLockPrismatic()
    prismatic_ground_ball.SetName("prismatic_ground_ball")
    prismatic_ground_ball.Initialize(body_floor, ball, chrono.ChCoordsysD(chrono.ChVectorD(5.5, 0, 0), z2x))
    my_system.AddLink(prismatic_ground_ball)
    
    my_system.AddLink(pt)
    my_system.Add(ball)
    obj.append(ball)    
    return (forceb,obj,ball)
# In[Wall]
def Wall(x,y,z,rotate,length,height,width,material,my_system):
    body_brick = chrono.ChBody()
    body_brick.SetBodyFixed(True)
    # set initial position
    body_brick.SetPos(chrono.ChVectorD(x,y,z))
    body_brick.SetRot(chrono.Q_from_AngY(rotate))
    # set mass properties
    body_brick.SetMass(.5)
    body_brick.SetInertiaXX(chrono.ChVectorD(1,1,1))
    # set collision surface properties
    body_brick.SetMaterialSurface(material)
    # Collision shape
    body_brick.GetCollisionModel().ClearModel()
    body_brick.GetCollisionModel().AddBox(length/2, height/2, width/2) # must set half sizes
    body_brick.GetCollisionModel().BuildModel()
    body_brick.SetCollide(True)
    # Visualization shape, for rendering animation
    body_brick_shape = chrono.ChBoxShape()
    body_brick_shape.GetBoxGeometry().Size = chrono.ChVectorD(length/2, height/2, width/2)
    col_brick = chrono.ChColorAsset()
    col_brick.SetColor(chrono.ChColor(1,0.1,0.5))     #Pink
    body_brick.AddAsset(col_brick)
    body_brick.GetAssets().push_back(body_brick_shape)
    body_brick.GetAssets().push_back(col_brick)
    my_system.Add(body_brick)    
 
    # Radius inside
# In[Call springs to be jammed]
def Jamsprings(k,rl,rlmax,Springs,t,tj,Fm,kj,rlj,rljmax,i,jamcall):
    if t>tj:
        if jamcall==1:
     
            k=kj

            rl=rlj
            rlmax=rljmax
        else:
            k=k
            rl=rl
            rlmax=rlmax
    else:
        k=k
        rl=rl
        rlmax=rlmax
        
    return(k,rlmax,rl)
# In[Set springs]   
def setSpring(k,rl,rlmax,Springs,Fm,templ,i):
    var1=Springs[i].Get_SpringLength()
    if var1<rl:
        Springs[i].Set_SpringF(k)
        var2=Springs[i].Get_SpringF()    
    if var1>rlmax:
        Springs[i].Set_SpringF(10*k)
        var2=Springs[i].Get_SpringF()    
    else:
        Springs[i].Set_SpringF(k)
        var2=Springs[i].Get_SpringF()    
  
    Fm.append(var2)
    templ.append(var1)
    return(Springs,Fm,templ)


# In[Extract data] 
def ExtractData(obj,nb,nt,Xpos,Ypos,Zpos,Xforce,Yforce,Zforce,rott0,rott1,rott2,rott3,Xvel,Yvel,Zvel,ballp,Balls,Springs,Fm,templ):
       
    for i in range(nb):
        var1=(Springs[i].Get_SpringLength()-Springs[i].Get_SpringRestLength())
        var2=Springs[i].Get_SpringK()
        var3=Springs[i].Get_SpringVelocity()
        var4=Springs[i].Get_SpringR()              
        Fm.append(var1*var2+var3*var4)
        templ.append(var1)
    
    for i in range(nt):
        tempx=obj[i].Get_Xforce()
        tempxx=obj[i].GetPos_dt()
        
        # Total forces
        Xforce.append(tempx.x)
        Yforce.append((tempx.y))
        Zforce.append(tempx.z)
            
        #positions
        Xpos.append(obj[i].GetPos().x)
        Ypos.append((obj[i].GetPos().y))
        Zpos.append(obj[i].GetPos().z)
        
        # Rotation positions
        rott0.append(obj[i].GetRot().e0)
        rott1.append(obj[i].GetRot().e1)
        rott2.append(obj[i].GetRot().e2)
        rott3.append(obj[i].GetRot().e3)
            
   
        # fill in the temporary velocity matrices
        Xvel.append(tempxx.x)
        Yvel.append(tempxx.y)
        Zvel.append(tempxx.z)

    # pull data for ball                      
    for i in range(len(Balls)):
        ballp.append(Balls[i].GetPos().x)   

    return (obj,Springs,Fm,ballp,Balls,templ,Xforce,Yforce,Zforce,Xpos,Ypos,Zpos,rott0,rott1,rott2,rott3,Xvel,Yvel,Zvel)



# In[Centroid]
def centroid(obj,nb,Xpos,Ypos,Zpos,tj,t,tstep):
    
    if t>tj:
        i=tj/tstep
        i=int(i)
    else:
        i=t/tstep
        i=int(i)
        
    Xpos2=np.asarray(Xpos)
    Zpos2=np.asarray(Zpos)
   
    Xpostemp=Xpos2[nb*i:nb*i+nb]
    Zpostemp=Zpos2[nb*i:nb*i+nb]
    Xavg=np.mean(Xpostemp)
    Zavg=np.mean(Zpostemp)
    return (Xavg,Zavg)                   

def centroid2(obj,nb):
    for i in range(nb):
        Xpostemp=obj[i].GetPos().x
        Zpostemp=obj[i].GetPos().z
        
    Xavg=np.sum(Xpostemp)/(nb)
    Zavg=np.sum(Zpostemp)/(nb)
    return (Xavg,Zavg)
# In[Define direction]
    
def direction(Xavg,Zavg,obj,ii,height):
    xbar=Xavg-obj[ii].GetPos().x
    zbar=Zavg-obj[ii].GetPos().z
    magn=np.sqrt((xbar**2)+(zbar**2))
    xd=xbar/magn
    zd=zbar/magn
    yd=height/2
    
    return(xd,yd,zd)
    
# In[Controller]
def Controller(my_system,force,mag,botcall,tset,Fm,nb,k,templ,t,rl,rlmax,Springs,forceb,jamcall,rlj,rljmax,tj,kj,mag2,magf,tp,obj,mag3,Xpos,Ypos,Zpos,tstep,height,xx,zz):
           
    for ii in range(nb):
        
        #(Xavg,Zavg)=centroid(obj,nb,Xpos,Ypos,Zpos,tj,t,tstep)
        #xx.append(Xavg)
        #zz.append(Zavg)
        (k1,rlmax1,rl1)=Jamsprings(k,rl,rlmax,Springs,t,tj,Fm,kj,rlj,rljmax,ii,jamcall[0,ii])
           
        (Springs,Fm,templ)=setSpring(k1,rl1,rlmax1,Springs,Fm,templ,ii)   
        # if past settling time  
        if t > tset:
            for j in range(len(force)):
                force[j].SetMforce(mag)
                force[j].SetDir(chrono.VECT_X)
            for j in range(len(forceb)):
                forceb[j].SetMforce(-mag2)
                forceb[j].SetDir(chrono.ChVectorD(1,0,0))
        # if past jamming time 
        if t > tj:
            
            (Xavg,Zavg)=centroid2(obj,nb)
            #(Xavg,Zavg)=centroid(obj,nb,Xpos,Ypos,Zpos,tj,tstep)
            #xx.append(Xavg)
            #zz.append(Zavg)
            (xd,yd,zd)=direction(Xavg,Zavg,obj,ii,height)
            
            for l in range(len(force)):
                force[l].SetMforce(mag3)
                force[l].SetDir(chrono.ChVectorD(xd,yd,zd))
                
            for l in range(len(forceb)):
                forceb[l].SetMforce(-mag2) 
                forceb[l].SetDir(chrono.ChVectorD(1,0,0))
    
        # i past pulling time
        if t>tp:
            (Xavg,Zavg)=centroid2(obj,nb)
            (xd,yd,zd)=direction(Xavg,Zavg,obj,ii,height)
            for l in range(len(force)):
                force[l].SetMforce(mag3)
                force[l].SetDir(chrono.ChVectorD(xd,yd,zd))
            for l in range(len(forceb)):
                forceb[l].SetMforce(1.5*magf)
                forceb[l].SetDir(chrono.ChVectorD(1,0,0))   
    return (force,Springs,templ,Fm,forceb,my_system,xx,zz)
# In[Controls] DONT CONVERT ME 
class PidControl:
    def __init__(self, nb, mag, tar, XdirForce, YdirForce,ZdirForce,XposBody,YposBody,ZposBody,Kp, Ki, Kd):
        self.nb = nb
        self.mag = mag
        self.tar = tar
        #directions
        self.XdirForce = XdirForce
        self.YdirForce = YdirForce
        self.ZdirForce = ZdirForce
        self.XposBody = XposBody
        self.YposBody = YposBody
        self.ZposBody = ZposBody
        self.angle_threshold = 2 
        self.pos_threshold  = 0.1
        # control factors
        #self.Kp=Kp
        #self.Ki=Ki
        #self.Kd=Kd
        
        # Initialize the controllers
        self.pid = PID(p=Kp, i=Ki, d=Kd)
     
        self.Tlocation = [self.tar[0],self.tar[1],self.tar[2]]
        self.dirF = [self.XdirForce,self.YdirForce,self.YdirForce]
    
        self.XcenPos = XposBody
        self.YcenPos = YposBody
        self.ZcenPos = ZposBody
        
        self.cur_angle = self.start_control_loop()
        self.intarget = self.reaching_target()
        
    def set_curangle(self):
        self.cur_angle = np.arctan(self.dirF[2]/self.dirF[0])
        self.cur_angle = np.degrees(self.cur_angle)
        return self.cur_angle
        
            
    def set_target(self):
        Zaxis = self.Tlocation[2] - self.ZcenPos
        Xaxis = self.Tlocation[0] - self.XcenPos
        tar_angle = np.degrees(np.arctan((Zaxis/Xaxis)))
        
        if Zaxis > 0.01:    
            if -0.01<= Xaxis <= 0.01:
               return 90
            if Xaxis < -0.01:
               return 180 + tar_angle
            if Xaxis > 0.01:
               return tar_angle
           
        if Zaxis < -0.01:
            if -0.01 <= Xaxis <= 0.01:
               return -90
            if Xaxis > 0.01:
               return tar_angle
            if Xaxis < -0.01:
               return tar_angle-180
           
        if -0.01 <= Zaxis <= 0.01:
            if -0.01 <= Xaxis <= 0.01:
               return 0
            if Xaxis < -0.01:
               return 180
            if Xaxis > 0.01:
               return 0  
                 
  
    def start_control_loop(self):
        while True:
            tar_angle = self.set_target()
            self.pid.target = tar_angle
            self.cur_angle = self.set_curangle()
            while True:
                self.pid(feedback=self.cur_angle)
                error = self.pid.error
                angles_are_set = (abs(error) < self.angle_threshold)  # Verify if every angles are within the tolerances
                if angles_are_set:
                    return self.cur_angle
                    break
                else:
                    if tar_angle >= 0:
                        self.cur_angle = self.cur_angle + 2   
                    
                    else:
                        self.cur_angle = self.cur_angle - 2
                        
                        
    def reaching_target(self):

        
        dis = np.sqrt((self.XposBody-self.tar[0])**2 + (self.ZposBody-self.tar[2])**2)
        
            
        if dis<self.pos_threshold:
            return 0
        else: 
            return self.mag


# In[Extract Data 3 this has controls in it ] DONT CONVERT ME
def ExtractData3(my_system,obj,i,force,forceb,mag,mag2,magf,rl,rlj,
                 rlmax,rljmax,Springs,Fm,k,ballp,Balls,templ,Xforce,
                 Yforce,Zforce,Xcontact,Ycontact,Zcontact,Xpos,Ypos,Zpos,
                 rott0,rott1,rott2,rott3,Xvel,Yvel,Zvel,nb,nt,t,tj,tset,tp,
                 jamcall,kj,XdirForce,YdirForce,ZdirForce,tarpos,Kp,Ki,Kd):
    
    # Apply forces to robots
    for i in range(len(force)):
        force[i].SetDir(chrono.VECT_X)

        
    for i in range(nb):
        
        # if past settling time  
        if t > tset:
            for i in range(len(force)):
                force[i].SetMforce(mag)


                # Control
                # target positon of robot i 
                tar=tarpos[i,:]
                x=obj[i].GetPos().x
                y=obj[i].GetPos().y
                z=obj[i].GetPos().z
                control = PidControl(nb,mag,tar,XdirForce,YdirForce,ZdirForce,x,y,z,Kp,Ki,Kd)
                forcemag = control.intarget
                force[i].SetDir(chrono.ChVectorD(np.cos(np.radians(control.cur_angle)),0,np.sin(np.radians(control.cur_angle))))
                force[i].SetMforce(forcemag)
                    
                    
            for i in range(len(forceb)):
                forceb[i].SetMforce(-mag2)
                forceb[i].SetDir(chrono.ChVectorD(1,0,0))
        # if past jamming time 
        if t > tj:
            for i in range(len(force)):
                force[i].SetMforce(mag)
        
            for i in range(len(forceb)):
                forceb[i].SetMforce(-mag2) 
                forceb[i].SetDir(chrono.ChVectorD(1,0,0))
        # if past pulling time
        if t>tp:
            for i in range(len(force)):
                force[i].SetMforce(mag)
            for i in range(len(forceb)):
                forceb[i].SetMforce(1*magf)
                forceb[i].SetDir(chrono.ChVectorD(1,0,0))   
# Jam certain robots if jam time is hit
        if t>tj:
            if jamcall[:,i]==1:
                rl=rlj
                rlmax=rljmax
  
                Springs[i].Set_SpringF(kj)
            
            else:
                rl=rl
                rlmax=rlmax
            
         #Check if springs are correct length otherwise adjust            
        var1=Springs[i].Get_SpringLength()
        if var1<rl:
            Springs[i].Set_SpringF(0)
            var2=0
        if var1>rlmax:
            Springs[i].Set_SpringF(2*k)
            var2=2*k
        else:
            Springs[i].Set_SpringF(k)
            var2=k
    
    for i in range(nb):
        
        var1=(Springs[i].Get_SpringLength()-Springs[i].Get_SpringRestLength())
        var2=Springs[i].Get_SpringF()
        Fm.append(var2)
        templ.append(var1)
        

# Pull data for plots                  
    for i in range(nt):
            
        templ.append(var1)
        temp=obj[i].GetContactForce()
        tempx=obj[i].Get_Xforce()
        tempxx=obj[i].GetPos_dt()
         
            
                       
        Xforce.append(tempx.x)
        Yforce.append((tempx.y))
        Zforce.append(tempx.z)
            
        Xcontact.append(temp.x)
        Ycontact.append(temp.y)
        Zcontact.append(temp.z)
            
        Xpos.append(obj[i].GetPos().x)
        Ypos.append((obj[i].GetPos().y))
        Zpos.append(obj[i].GetPos().z)
            
        rott0.append(obj[i].GetRot().e0)
        rott1.append(obj[i].GetRot().e1)
        rott2.append(obj[i].GetRot().e2)
        rott3.append(obj[i].GetRot().e3)
            
   
        # fill in the temporary velocity matrices
        Xvel.append(tempxx.x)
        Yvel.append(tempxx.y)
        Zvel.append(tempxx.z)

    # pull data for ball                      
    for i in range(len(Balls)):
        ballp.append(Balls[i].GetPos().x)   

    return (my_system,obj,force,forceb,Springs,Fm,k,ballp,Balls,templ,Xforce,Yforce,Zforce,Xcontact,Ycontact,Zcontact,Xpos,Ypos,Zpos,rott0,rott1,rott2,rott3,Xvel,Yvel,Zvel)


# In[Number of contacts]
    
class MyReportContactCallback(chrono.ReportContactCallback):

    def __init__(self):

        chrono.ReportContactCallback.__init__(self)
        self.Fcx=[]
        self.Fcy=[]
        self.Fcz=[]
        self.pointx = []
        self.pointy = []
        self.pointz = []
        #self.bodies = []
    def OnReportContact(self,vA,vB,cA,dist,rad,force,torque,modA,modB):
#        bodyUpA = chrono.CastContactableToChBody(modA)
#        nameA = bodyUpA.GetId()
#        bodyUpB = chrono.CastContactableToChBody(modB)
#        nameB = bodyUpB.GetId()
        self.pointx.append(vA.x)
        self.pointy.append(vA.y)
        self.pointz.append(vA.z)
        self.Fcx.append(force.x)
        self.Fcy.append(force.y)
        self.Fcz.append(force.z)
        #self.bodies.append([nameA,nameB])
        return True        # return False to stop reporting contacts

    # reset after every run 
    def ResetList(self):
        self.pointx = []
        self.pointy = []
        self.pointz = [] 
        self.Fcx=[]
        self.Fcy=[]
        self.Fcz=[]
    # Get the points
    def GetList(self):
        return (self.pointx,self.pointy,self.pointz,self.Fcx,self.Fcy,self.Fcz)
    

# In[Controls]




   
# def interior_radii(nb,diameter,R1):

#     #nb=100             # number of robots
#     #diameter=.07        # diameter of cylinder and robots
#     #R1=(diameter*nb/(np.pi*2))+.1 
#     Rin=R1-diameter/2  
#     ngrans1=int(Rin/diameter)


#     ri=np.zeros((1,ngrans1))
#     ni=np.zeros((1,ngrans1))

    

#     for i in range(ngrans1):
    
#         ri[:,i]=Rin-i*diameter/2
#         ni[:,i]=np.floor((ri[:,i]*np.pi**2)/diameter)
#     ni=np.asarray(ni,dtype=int)
            
#     return ni

        

# In[Extract data] DONT CONVERT ME
#def ExtractData(my_system,obj,i,force,forceb,mag,mag2,magf,rl,rlj,rlmax,rljmax,Springs,Fm,k,ballp,Balls,templ,Xforce,Yforce,Zforce,Xcontact,Ycontact,Zcontact,Xpos,Ypos,Zpos,rott0,rott1,rott2,rott3,Xvel,Yvel,Zvel,nb,nt,t,tj,tset,tp,jamcall,kj):
#    
#    # Apply forces to robots
#    for i in range(len(force)):
#        force[i].SetDir(chrono.VECT_X)
#    
#    for i in range(nb):
#        # if past settling time  
#        if t > tset:
#            for i in range(len(force)):
#                force[i].SetMforce(mag)
#            for i in range(len(forceb)):
#                forceb[i].SetMforce(-mag2)
#                forceb[i].SetDir(chrono.ChVectorD(1,0,0))
#        # if past jamming time 
#        if t > tj:
#            for i in range(len(force)):
#                force[i].SetMforce(mag)
#        
#            for i in range(len(forceb)):
#                forceb[i].SetMforce(-mag2) 
#                forceb[i].SetDir(chrono.ChVectorD(1,0,0))
#        if t>tp:
#            for i in range(len(force)):
#                force[i].SetMforce(mag)
#            for i in range(len(forceb)):
#                forceb[i].SetMforce(1*magf)
#                forceb[i].SetDir(chrono.ChVectorD(1,0,0))   
## Jam certain robots if jam time is hit
#        if t>tj:
#            if jamcall[:,i]==1:
#                rl=rlj
#                rlmax=rljmax
#  
#                Springs[i].Set_SpringF(kj)
#            
#            else:
#                rl=rl
#                rlmax=rlmax
#            
#         #Check if spirngs are correct length otherwise adjust            
#        var1=Springs[i].Get_SpringLength()
#        if var1<rl:
#            Springs[i].Set_SpringK(0)
#            var2=0
#        if var1>rlmax:
#            Springs[i].Set_SpringK(2*k)
#            var2=2*k
#        else:
#            Springs[i].Set_SpringK(k)
#            var2=k
#    
#    for i in range(nb):
#        
#        var1=(Springs[i].Get_SpringLength()-Springs[i].Get_SpringRestLength())
#        var2=Springs[i].Get_SpringK()
#        var3=Springs[i].Get_SpringVelocity()
#        var4=Springs[i].Get_SpringR()              
#        Fm.append(var1*var2+var3*var4)
#        templ.append(var1)
#         
#
## Pull data for plots                  
#    for i in range(nt):
#            
#        templ.append(var1)
#        temp=obj[i].GetContactForce()
#        tempx=obj[i].Get_Xforce()
#        tempxx=obj[i].GetPos_dt()
#         
#            
#                       
#        Xforce.append(tempx.x)
#        Yforce.append((tempx.y))
#        Zforce.append(tempx.z)
#            
#        Xcontact.append(temp.x)
#        Ycontact.append(temp.y)
#        Zcontact.append(temp.z)
#            
#        Xpos.append(obj[i].GetPos().x)
#        Ypos.append((obj[i].GetPos().y))
#        Zpos.append(obj[i].GetPos().z)
#        
#        XdirForce.append(dirForce.x)
#        YdirForce.append(dirForce.y)
#        ZdirForce.append(dirForce.z)
#        
#        rott0.append(obj[i].GetRot().e0)
#        rott1.append(obj[i].GetRot().e1)
#        rott2.append(obj[i].GetRot().e2)
#        rott3.append(obj[i].GetRot().e3)
#            
#   
#        # fill in the temporary velocity matrices
#        Xvel.append(tempxx.x)
#        Yvel.append(tempxx.y)
#        Zvel.append(tempxx.z)
#
#    # pull data for ball                      
#    for i in range(len(Balls)):
#        ballp.append(Balls[i].GetPos().x)   
#
#    return (my_system,obj,force,forceb,Springs,Fm,k,ballp,Balls,templ,Xforce,Yforce,Zforce,Xcontact,Ycontact,Zcontact,Xpos,Ypos,Zpos,rott0,rott1,rott2,rott3,Xvel,Yvel,Zvel)


            
# In[Extract const force]

#def ExtractData2(my_system,obj,i,force,forceb,mag,mag2,magf,rl,rlj,rlmax,rljmax,Springs,Fm,k,ballp,Balls,templ,Xforce,Yforce,Zforce,Xcontact,Ycontact,Zcontact,Xpos,Ypos,Zpos,rott0,rott1,rott2,rott3,Xvel,Yvel,Zvel,nb,nt,t,tj,tset,tp,jamcall,kj):
#    
#    # Apply forces to robots
#    for i in range(len(force)):
#        force[i].SetDir(chrono.VECT_X)
#    
#    for i in range(nb):
#        (k1,rlmax1,rl1)=Jamsprings(k,rl,rlmax,Springs,t,tj,Fm,kj,rlj,rljmax,i,jamcall[0,i])
#         
#        
#           
#        (Springs,Fm,templ)=setSpring(k1,rl1,rlmax1,Springs,Fm,templ,i)   
#        # if past settling time  
#        if t > tset:
#            for i in range(len(force)):
#                force[i].SetMforce(mag)
#            for j in range(len(forceb)):
#                forceb[j].SetMforce(-mag2)
#                forceb[j].SetDir(chrono.ChVectorD(1,0,0))
#        # if past jamming time 
#        if t > tj:
#            for i in range(len(force)):
#                force[i].SetMforce(mag)
#        
#            for j in range(len(forceb)):
#                forceb[j].SetMforce(-mag2) 
#                forceb[j].SetDir(chrono.ChVectorD(1,0,0))
#        # if past pulling time
#        if t>tp:
#            for i in range(len(force)):
#                force[i].SetMforce(mag)
#            for j in range(len(forceb)):
#                forceb[j].SetMforce(1*magf)
#                forceb[j].SetDir(chrono.ChVectorD(1,0,0))   
#
#
## Pull data for plots                  
#    for i in range(nt):
#            
#        
#        temp=obj[i].GetContactForce()
#        tempx=obj[i].Get_Xforce()
#        tempxx=obj[i].GetPos_dt()
#         
#            
#                       
#        Xforce.append(tempx.x)
#        Yforce.append((tempx.y))
#        Zforce.append(tempx.z)
#            
#        Xcontact.append(temp.x)
#        Ycontact.append(temp.y)
#        Zcontact.append(temp.z)
#            
#        Xpos.append(obj[i].GetPos().x)
#        Ypos.append((obj[i].GetPos().y))
#        Zpos.append(obj[i].GetPos().z)
#            
#        rott0.append(obj[i].GetRot().e0)
#        rott1.append(obj[i].GetRot().e1)
#        rott2.append(obj[i].GetRot().e2)
#        rott3.append(obj[i].GetRot().e3)
#            
#   
#        # fill in the temporary velocity matrices
#        Xvel.append(tempxx.x)
#        Yvel.append(tempxx.y)
#        Zvel.append(tempxx.z)
#
#    # pull data for ball                      
#    for i in range(len(Balls)):
#        ballp.append(Balls[i].GetPos().x)   
#
#    return (my_system,obj,force,forceb,Springs,Fm,k,ballp,Balls,templ,Xforce,Yforce,Zforce,Xcontact,Ycontact,Zcontact,Xpos,Ypos,Zpos,rott0,rott1,rott2,rott3,Xvel,Yvel,Zvel)
#

