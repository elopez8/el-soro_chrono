# -*- coding: utf-8 -*-
"""
Created on Mon Apr 27 19:56:00 2020

@author: dmulr
"""
import numpy as np
import pychrono.core as chrono
import pychrono.irrlicht as chronoirr
import pychrono.postprocess as postprocess
import os
import numpy as np
import timeit


visual="irrlecht"
sim='6'
type_spring="var"
#control_type="nothing"
#control_type="force_right"
control_type="pot_field"
fixed=False# is it fixed
#mode='nmax'  # will there be interiors
#mode='empty'
mode='max'
# add a colar for grabbing mode
colar_mode=False


mc=.1
lc=100
kc=1000
fc=mc*lc
tstep=.005   # time step
tend=100     # time end

mu_f=.3    # friction
mu_b=.01    # dampning
mu_r=.1     # rolling friction
mu_s=.01    # SPinning fiction

Ct=.0001
C=.000000001
Cr=.0001
Cs=.0001

mr=.120       # mass
mp=.06
nb=50      # number of robots

height=.06  # height of cylinder
diameter=.035 # diameter of cylinder and robots

volume=np.pi*.25*height*(diameter)**2   # calculate volume


diameter2=.035 # diameter of cylinder and robots
volume2=np.pi*.25*height*(diameter2)**2   # calculate volume


rowr=mr/volume # calculate density of robot
rowp=mp/volume2

mag=2
k=400   # spring constant (bots)
rl=0
rlmax=0
length=8  # Length of the body floor
tall=1     # height of the body floor
obj=[]
data_path="C:/Users/dmulr/OneDrive/Documents/data/"

   

#shape="circle"
shape="Square"
#shape="grab"
alpha=300
beta=10

R=(diameter*nb/(np.pi*2))+.1 
Rd=4*R

bl=-1
br=1
p1=0
p2=0

# Ring values based on shape
if shape=="circle":
    nr=[0,1,2,3,4] # circle
    
if shape=="Square":
    nr=[.3,.4,.5,.6] # square
    
if shape=="grab":
    nr=[]