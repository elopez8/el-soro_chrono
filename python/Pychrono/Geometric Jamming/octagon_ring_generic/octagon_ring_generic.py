#general code for comprssion testing of rings of regular 2D nodes (jammed or unjammed)

import pychrono as chrono 
import builtins
import numpy as np
import sys
import pychrono.irrlicht as chronoirr

def main(
    r_i=0.025,
    h=0.01,
    N_s=8,
    N_n=25,
    body_friction=1.2,
    ground_friction=0.01,
    block_friction=0.1,
    r=0.1,
    k=500,
    jammed=True,
    strain_rate = 0.01,
    strain = 0.2
):
    m_timestep = 0.00001
    #m_length = 15.0
    m_visualization = "irrlicht"

    r_o = r_i / np.cos(np.pi / N_s)
    R_o = r_o / np.sin(np.pi / N_n)
    x_o = 2*r_i
    cam_y = 5*R_o
    if not jammed:
        x_o = 2*r_o
    m_length = (R_o)/strain_rate

    polygon_texture = chrono.ChTexture()
    polygon_texture.SetTextureFilename('.\data\octB.png')
    ground_texture = chrono.ChTexture()
    ground_texture.SetTextureFilename('.\data\ground.png')
    block_texture = chrono.ChTexture()
    block_texture.SetTextureFilename('.\data\octA.png')

    bodies = get_ring(N_s=N_s, r_i=r_i, h=h, density=2414.3, N_n=N_n, texture=polygon_texture)
    springs = add_ring_springs(bodies, x_o, r, k)
    ground = get_ground(6*R_o, 2*r_i, 6*R_o, 10000, ground_texture)
    block_fixed = get_block_fixed(R_o, 3*R_o, h, r_o, 10000, block_texture)
    block_moving = get_block_moving(R_o, 3*R_o, h, r_o, 10000, block_texture)

    block_vel = chrono.ChVectorD(0, 0, strain_rate)

    link = chrono.ChLinkMateParallel()
    cA = chrono.ChVectorD(0, 0, 0)
    dA = chrono.ChVectorD(1, 0, 0)
    cB = chrono.ChVectorD(0, 0, 0)
    dB = chrono.ChVectorD(1, 0, 0)
    link.Initialize(block_moving, block_fixed, False, cA, cB, dA, dB)

    for body in bodies:
        body.GetMaterialSurfaceNSC().SetFriction(body_friction)
        body.GetMaterialSurfaceNSC().SetRollingFriction(0.0)
    ground.GetMaterialSurfaceNSC().SetFriction(ground_friction)
    ground.GetMaterialSurfaceNSC().SetRollingFriction(0.0)
    block_fixed.GetMaterialSurfaceNSC().SetFriction(block_friction)
    block_fixed.GetMaterialSurfaceNSC().SetRollingFriction(0.0)
    block_moving.GetMaterialSurfaceNSC().SetFriction(block_friction)
    block_moving.GetMaterialSurfaceNSC().SetRollingFriction(0.0)

    my_system = chrono.ChSystemNSC()
    for body in ([block_fixed, block_moving, link, ground] + bodies + springs):
        my_system.Add(body)
    my_system.SetSolverTolerance(1e-9)
    solver = chrono.ChSolverBB()
    my_system.SetSolver(solver)
    solver.SetMaxIterations(300)
    chrono.ChCollisionModel_SetDefaultSuggestedEnvelope(0.001)

    csv_file = 'data.csv'
    stress_strain_file = open(csv_file, 'w')
    apf=[]
    apc=[]
    if m_visualization == "irrlicht":
        #  Create an Irrlicht application to visualize the system
        myapplication = chronoirr.ChIrrApp(my_system, 'Octagon', chronoirr.dimension2du(1280, 720))
        myapplication.AddTypicalSky(chrono.GetChronoDataPath() + 'skybox/')
        myapplication.AddTypicalCamera(chronoirr.vector3df(0, cam_y, 0), chronoirr.vector3df(0.0, 0.0, 0.0))
        myapplication.AddLightWithShadow(chronoirr.vector3df(2, 5, 2), chronoirr.vector3df(2, 2, 2), 10, 2, 10, 120)
        myapplication.SetSymbolscale(0.002)
        myapplication.SetShowInfos(True)
        myapplication.SetContactsDrawMode(2)
        myapplication.AssetBindAll()
        myapplication.AssetUpdateAll()
        t = 0
        i = 0
        my_system.Set_G_acc(chrono.ChVectorD(0, -9.8, 0))
        z_0 = block_moving.GetPos().z
        while (myapplication.GetDevice().run()):
            block_moving.SetPos_dt(block_vel)
            app_force = block_moving.Get_Xforce()
            cont_force = block_moving.GetContactForce()
            cont_force_fixed = block_fixed.GetContactForce()
            print("%f,%f,%f" % (t, block_moving.GetPos().z - z_0, -(app_force + cont_force).z), file=stress_strain_file)
            apf.append(app_force)
            apc.append(cont_force) 
#            if i % 10 == 0:
#                print(t, block_moving.GetPos().z - z_0, -(app_force + cont_force - cont_force_fixed).z)
            myapplication.BeginScene()
            myapplication.DrawAll()
            myapplication.DoStep()
            myapplication.EndScene()
            t += m_timestep
            i += 1
            if (t >= m_length):
                # f.close()
                break
    strain_stress_file.close()

def get_polygon_body(N_s=8, r_i=0.025, h=0.01, density=2414.3, texture=None):
    pt_vect = chrono.vector_ChVectorD()
    r_o = r_i/np.cos(np.pi/N_s)
    eta = np.random.rand() * 2*np.pi/N_s
    for i in range(N_s):
        pt_vect.push_back(chrono.ChVectorD(
            r_o*np.cos(eta + i*2*np.pi/N_s),
            0,
            r_o*np.sin(eta + i*2*np.pi/N_s)
        ))
        pt_vect.push_back(chrono.ChVectorD(
            r_o * np.cos(eta + i*2*np.pi / N_s),
            h,
            r_o * np.sin(eta + i*2*np.pi / N_s)
        ))
    body = chrono.ChBodyEasyConvexHull(pt_vect, density, True, True)
    if texture is not None:
        body.AddAsset(texture)
    return body

def get_ring(N_s=8, r_i=0.025, h=0.01, density=2414.3, N_n=8, texture=None):
    ring_bodies = []
    r_o = r_i/np.cos(np.pi/N_s)
    R_o = r_o/np.sin(np.pi/N_n)
    eta = np.random.rand() * 2*np.pi/N_n
    for i in range(N_n):
        body = get_polygon_body(N_s, r_i, h, density, texture)
        body.SetPos(chrono.ChVectorD(R_o*np.cos(eta + i*2*np.pi/N_n), h/2, R_o*np.sin(eta + i*2*np.pi/N_n)))
        ring_bodies.append(body)
    return ring_bodies

def add_ring_springs(bodies, d=0.049, r=0.1, k=100):
    springs = []
    for body_A, body_B in zip(bodies, (bodies[1:] + [bodies[0]])):
        spring = chrono.ChLinkSpring()
        spring.Initialize(body_A, body_B, True, chrono.ChVectorD(0.0, 0, 0.0), chrono.ChVectorD(0.0, 0, 0.0), True)
        spring.Set_SpringRestLength(d)
        spring.Set_SpringR(r)
        spring.Set_SpringK(k)
        springs.append(spring)
    return springs

def get_ground(x_s, y_s, z_s, density=10000, texture=None):
    ground = chrono.ChBodyEasyBox(x_s, y_s, z_s, density, True, True)
    ground.SetPos(chrono.ChVectorD(0, -y_s/2, 0))
    ground.SetBodyFixed(True)
    if texture is not None:
        ground.AddAsset(texture)
    return ground

def get_block_fixed(R_o, x_s, y_s, z_s, density=100000, texture=None):
    block_fixed = chrono.ChBodyEasyBox(x_s, y_s, z_s, density, True, True)
    block_fixed.SetPos(chrono.ChVectorD(0, y_s/2, R_o+1.5*z_s))
    block_fixed.SetBodyFixed(True)
    if texture is not None:
        block_fixed.AddAsset(texture)
    return block_fixed

def get_block_moving(R_o, x_s, y_s, z_s, density=100000, texture=None):
    block_moving = chrono.ChBodyEasyBox(x_s, y_s, z_s, density, True, True)
    block_moving.SetPos(chrono.ChVectorD(0, y_s/2, -(R_o+1.5*z_s)))
    block_moving.SetBodyFixed(False)
    if texture is not None:
        block_moving.AddAsset(texture)
    return block_moving



if __name__ == '__main__':
    main()

sys.exit()

