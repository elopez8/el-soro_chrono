# -*- coding: utf-8 -*-
"""
Created on Tue Feb 11 13:22:36 2020

@author: Esteban Lopez

Source Code:
https://github.com/openai/gym/blob/master/gym/envs/classic_control/cartpole.py

Use this as a sample on setting up the cartpole problem.
"""

import Strings_Environment_Test
import numpy as np

experiment_name = "Ram that bitch"
data_collect = False # Collects data and reports it in .csv files for later investigation.
plot = False    # Plots your data immediately. Will NOT work if data_collect=False
render = True       # Do you want to see the simulation?
POV_Ray = False      # Note: Won't work if render=False

env = Strings_Environment_Test.Strings(data_collect=data_collect, experiment_name=experiment_name, plot=plot, POV_Ray=POV_Ray)
max_time = 2000

_ = env.reset()

action = np.zeros(2*env.num_bots)
forcex = 1 # Force applied in the x-direction

for a in range(2*env.num_bots):
    if a%2==0: # Only forces in the x-direction are applied
        action[a]=forcex

for t in range(max_time):
    env.render()
    observation, reward, done, info = env.step(action)
    
    if render:
        env.render()
    
    if done:
        break

if data_collect:
    env.data_export()

env.close()