# -*- coding: utf-8 -*-
"""
Created on Sun Mar 22 15:20:55 2020

@author: elopez8

Plotter for data generated by RL

"""

import pandas as pd
import matplotlib.pyplot as plt 

# In[Import CSV Files]
X_data = pd.read_csv('X_data.csv', header=None)
Y_data = pd.read_csv('Y_data.csv', header=None)
Z_data = pd.read_csv('Z_data.csv', header=None)

X_vel = pd.read_csv('X_vel_data.csv', header=None)
Y_vel = pd.read_csv('Y_vel_data.csv', header=None)
Z_vel = pd.read_csv('Z_vel_data.csv', header=None)

forces = pd.read_csv('force_data.csv', header=None)
actions = pd.read_csv('actions.csv', header=None)
#rewards = pd.read_csv('reward.csv', header=None)

# In[Plot X, Y, and Z data]

# Commonalities between all plotters
num_bots = len(X_data.columns-1) # The first column is timestep, so all other columns are bot data
col_list = list(X_data)
col_list.remove(0) # Stores the column numbers besides the first one. The first column is time data
xlabel='Time [sec]'
time_step = X_data[0]


# Plot X-COM Position Data
X_COM = X_data[col_list].sum(axis=1)/num_bots # Calculates the X-Pos of JAMoEBA's center

plt.figure('X-Pos')
plt.plot(time_step, X_COM)
plt.xlabel(xlabel)
plt.ylabel('X-Position [m]')
plt.title('X-Center Position')
plt.savefig('X-Center Position.jpg')

# Plot Y-COM Position Data
Y_COM = Y_data[col_list].sum(axis=1)/num_bots # Returns an array with teh 

plt.figure('Y-Pos')
plt.plot(time_step, Y_COM)
plt.xlabel(xlabel)
plt.ylabel('Y-Position [m]')
plt.title('Y-Center Position')
plt.savefig('Y-Center Position.jpg')

# Plot Z-COM Position Data
Z_COM = Z_data[col_list].sum(axis=1)/num_bots # Returns an array with teh 

plt.figure('Z-Pos')
plt.plot(time_step, Z_COM)
plt.xlabel(xlabel)
plt.ylabel('Z-Position [m]')
plt.title('Z-Center Position')
plt.savefig('Z-Center Position.jpg')

# Plot X-Vel data
plt.figure('X-Vel')
for i in col_list:
    plt.plot(time_step, X_vel[i], label = 'Bot'+str(i))
plt.xlabel(xlabel)
plt.ylabel('X Velocity [m/s]')
plt.title('X-Velocity')
plt.legend(loc='lower right')
plt.savefig('X-Velocty.jpg')

# Plot Y-Vel data
plt.figure('Y-Vel')
for i in col_list:
    plt.plot(time_step, Y_vel[i], label = 'Bot'+str(i))
plt.xlabel(xlabel)
plt.ylabel('Y Velocity [m/s]')
plt.title('Y-Velocity')
plt.legend(loc='lower right')
plt.savefig('Y-Velocty.jpg')

# Plot Z-Vel data
plt.figure('Z-Vel')
for i in col_list:
    plt.plot(time_step, Z_vel[i], label = 'Bot'+str(i))
plt.xlabel(xlabel)
plt.ylabel('Z Velocity [m/s]')
plt.title('Z-Velocity')
plt.legend(loc='lower right')
plt.savefig('Z-Velocty.jpg')

# In[Plot forces, actions, and rewards]

#Create a new col_list
col_list_2 = list(forces)
col_list_2.remove(0)

# Plot X and Z Forces for each bot, separately
bot=1
for i in col_list_2:
    if i%2!=0:
        plt.figure('Forces Bot ' + str(bot))
        plt.plot(time_step, forces[i], label='X-Force')
        plt.plot(time_step, forces[i+1], label='Z-Force')
        plt.xlabel(xlabel)
        plt.ylabel('Force [N]')
        plt.title('Forces on Bot ' + str(bot))
        plt.legend(loc='lower right')
        plt.savefig('Bot ' + str(bot) + ' Forces.jpg')
        
        bot += 1
        
