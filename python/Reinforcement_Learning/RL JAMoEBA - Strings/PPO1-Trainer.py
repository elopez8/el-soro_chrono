# -*- coding: utf-8 -*-
"""
Created on Thu Mar  5 22:20:55 2020

@author: elopez8
"""
import Strings_Environment as Strings
import os

from stable_baselines.common.policies import MlpPolicy
from stable_baselines import PPO1

experiment_name = 'Experiment_37'

tensorboard_log = experiment_name + " Tensorboard Log/"
if not os.path.exists(tensorboard_log):
    os.makedirs(tensorboard_log)

env = Strings.Strings(experiment_name=experiment_name)
model = PPO1(MlpPolicy, env, verbose=1, tensorboard_log=tensorboard_log)
model.learn(total_timesteps=1000000)
model.save('ppo1_Strings_' + experiment_name)
env.parameter_export()
env.close()