# -*- coding: utf-8 -*-
"""
Created on Mon Mar  2 09:39:29 2020

@author: 17088
"""

import gym
from stable_baselines import PPO1
import ChronoGymPendulum as ChronoPendulum
import ChronoGymAnt

#env = gym.make('CartPole-v1')
#model=PPO1.load('ppo1_cartpole.zip')

#env = ChronoPendulum.ChronoPendulum()
#model=PPO1.load('ppo1_ChronoPendulum.zip')

env = ChronoGymAnt.ChronoAnt()
model = PPO1.load('ppo1_ChronoAnt.zip')

obs=env.reset()

for i in range(100000):
    action, _states = model.predict(obs)
    obs, reward, done, info = env.step(action)
    env.render()
    
    if done:
        break
    
env.close()