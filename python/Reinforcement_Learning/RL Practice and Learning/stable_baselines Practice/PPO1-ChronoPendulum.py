# -*- coding: utf-8 -*-
"""
Created on Wed Mar  4 12:22:11 2020

@author: elopez8
"""

#import chtrain_pendulum as ChronoPendulum #This is the OLD Pendulum
import ChronoGymPendulum as ChronoPendulum # gym-chrono version of the pendulum

from stable_baselines.common.policies import MlpPolicy #Learn what this is
from stable_baselines import PPO1

env = ChronoPendulum.ChronoPendulum()
model = PPO1(MlpPolicy, env, verbose=1)
model.learn(total_timesteps=10000)
model.save('ppo1_ChronoPendulum')
env.close()